init:
	docker network create skeleton || true
	make install
	make build
	make database-init

install:
	docker-compose run --rm node npm install

build:
	docker-compose run --rm node npm run build:ts

start:
	docker-compose up -d

down:
	docker-compose down

database-init:
	docker-compose down -v
	make database-start
	sleep 5
	make database-migrate

database-start:
	docker-compose up -d postgres

database-migrate:
	make database-start
	docker-compose run --rm node npm run database:migrate