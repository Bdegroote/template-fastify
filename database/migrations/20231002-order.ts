import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('order', (table) => {
    table
      .uuid('id', { primaryKey: true })
      .defaultTo(knex.raw('uuid_generate_v4()'));
    table.jsonb('customer');
    table.text('externalId').index();
    table.timestamp('date');
    table.boolean('optin');
    table.jsonb('price');
    table.timestamp('createdAt').defaultTo(knex.fn.now());
  });

  await knex.schema.createTable('package', (table) => {
    table
      .uuid('id', { primaryKey: true })
      .defaultTo(knex.raw('uuid_generate_v4()'));
    table.string('label');
    table.jsonb('length');
    table.jsonb('width');
    table.jsonb('height');
    table.jsonb('weight');
    table.uuid('orderId').references('order.id').onDelete('CASCADE');
  });
}

export async function down(): Promise<void> {}
