import { KnexRepository } from '../knex/knex.repository'
import { Order } from './interfaces/order.interface'

export class OrdersRepository extends KnexRepository<Order> {
  public tableName = 'order'
}

export default new OrdersRepository()
