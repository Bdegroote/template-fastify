import { FastifyReply, FastifyRequest } from 'fastify'
import { Order, OrderWithPackage } from './interfaces/order.interface'
import { OrdersService } from './orders.service'

export class OrdersController {
  private readonly service: OrdersService
  constructor() {
    this.service = new OrdersService()
  }

  async create(
    req: FastifyRequest<{ Body: OrderWithPackage }>,
    res: FastifyReply,
  ) {
    const { body: order } = req
    const newOrder = await this.service.create(order)

    await res.code(201).send({ order: newOrder })
  }

  async getAll(): Promise<{ orders: Order[] }> {
    const orders = await this.service.getAll()

    return { orders }
  }

  async getById(
    req: FastifyRequest<{ Params: { id: string } }>,
    res: FastifyReply,
  ) {
    const {
      params: { id },
    } = req
    const order = await this.service.getById(id)

    if (!order) {
      throw new Error('Order not found')
    }

    await res.code(200).send({ order })
  }

  async updateById(
    req: FastifyRequest<{ Params: { id: string }; Body: Partial<Order> }>,
    res: FastifyReply,
  ) {
    const {
      params: { id },
      body: order,
    } = req

    const updatedOrder = await this.service.updateById(id, order)

    if (!updatedOrder) {
      throw new Error('Order not found')
    }

    await res.code(200).send({ order: updatedOrder })
  }

  async deleteById(
    req: FastifyRequest<{ Params: { id: string } }>,
    res: FastifyReply,
  ): Promise<void> {
    const {
      params: { id },
    } = req

    const updatedOrder = await this.service.deleteById(id)

    if (!updatedOrder) {
      throw new Error('Order not found')
    }

    await res.code(200)
  }
}

export default new OrdersController()
