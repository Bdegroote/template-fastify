import { Package } from '../../packages/interfaces/package.interface';

export interface Order {
  id: string;
  customer: Record<string, any>;
  externalId: string;
  date: string;
  price: Record<string, any>;
  optin: boolean;
}

export type OrderWithPackage = Order & { packages: Package[] };
