import { FastifyPluginAsync } from 'fastify'
import ordersController from './orders.controller'

const orderRoute: FastifyPluginAsync = async (fastify): Promise<void> => {
  fastify.get('/orders', async (_req, _res) => await ordersController.getAll())
}

export default orderRoute
