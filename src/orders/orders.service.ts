import { PackagesService } from '../packages/packages.service'
import { Order, OrderWithPackage } from './interfaces/order.interface'
import { OrdersRepository } from './orders.repository'

export class OrdersService {
  private readonly repository: OrdersRepository
  private readonly packageService: PackagesService

  constructor() {
    this.repository = new OrdersRepository()
    this.packageService = new PackagesService()
  }

  async create({
    packages,
    ...order
  }: OrderWithPackage): Promise<OrderWithPackage> {
    const newOrder = await this.repository.insert(order)

    const newPackages = await this.packageService.createMany(
      packages.map((pack) => ({ ...pack, orderId: newOrder.id })),
    )

    return { ...newOrder, packages: newPackages }
  }

  async getAll(): Promise<Order[]> {
    return this.repository.find()
  }

  async getById(id: string): Promise<OrderWithPackage | undefined> {
    const order = await this.repository.findById(id)

    if (!order) {
      return
    }

    const packages = await this.packageService.getAll({ orderId: order.id })

    return { ...order, packages }
  }

  async updateById(
    id: string,
    { packages, ...order }: Partial<OrderWithPackage>,
  ): Promise<OrderWithPackage | undefined> {
    const updatedOrder = await this.repository.updateById(id, order)

    if (!updatedOrder) {
      return
    }

    await this.packageService.delete({ orderId: id })
    const newPackages = await this.packageService.createMany(
      packages!.map((pack) => ({ ...pack, orderId: id })),
    )

    return { ...updatedOrder, packages: newPackages }
  }

  async deleteById(id: string): Promise<number> {
    return this.repository.deleteById(id)
  }
}
