import { KnexRepository } from '../knex/knex.repository'

export class PackagesRepository extends KnexRepository {
  public tableName = 'package'
}

export default new PackagesRepository()
