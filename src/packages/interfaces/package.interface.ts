interface Unit {
  value: string;
  unit: string;
}

export interface Package {
  label: string;
  length: Unit;
  width: Unit;
  height: Unit;
  weight: Unit;
  orderId: string;
}

export interface PackageFilters {
  orderId: string;
}
