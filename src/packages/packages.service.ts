import { Package, PackageFilters } from './interfaces/package.interface'
import { PackagesRepository } from './packages.repository'

export class PackagesService {
  private readonly repository: PackagesRepository

  constructor() {
    this.repository = new PackagesRepository()
  }

  getAll(filters: PackageFilters) {
    return this.repository.find(filters)
  }

  async createMany(packages: Package[]): Promise<Package[]> {
    return this.repository.insertMany(packages)
  }

  async delete(filters: PackageFilters): Promise<void> {
    await this.repository.delete(filters)
  }
}
