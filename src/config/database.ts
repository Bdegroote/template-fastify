export default {
  client: 'pg',
  version: '13',
  connection: {
    user: 'postgres',
    password: 'postgres',
    database: 'database',
    host: 'postgres',
    port: '5432',
  },
  debug: true,
  migrations: {
    tableName: 'knex_migration',
    directory: '../../database/migrations',
  },
}
